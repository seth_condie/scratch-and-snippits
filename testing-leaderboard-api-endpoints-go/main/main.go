package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

func main() {
	var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImlzcyI6IklETSJ9.eyJpc3MiOiJJRE0iLCJpYXQiOjE1MzkzODgzNTEsImV4cCI6MTUzOTQ1MzE1MSwiaWRtaWQiOjUzMDk1LCJpZG1HdWlkIjoiMTQyMTgzYTgtMzBhZi00MjI5LWFiMGYtZWI0NDMxOTM3MzZmIiwiY3JtdHlwZSI6IlNGREMiLCJzdGFzdWIiOiJxZXVzcG9kMiIsIm9yZ2lkIjoxNzAxMDgwMywiY3JtdWlkIjoiMDA1NDYwMDAwMDBnTUFHQUEyIiwiYWJsIjpmYWxzZSwidHlrX2tleV9wb2xpY3lfaWQiOiI1Nzk2NjM3MjQ0MDNhYjVhYzUwMDAwMDEiLCJsaWNlbnNlcyI6W10sImNhcGFiaWxpdGllcyI6WyJQbGF5Ym9va3MiLCJWaXNpb24iLCJJbnNpZ2h0cyIsIlBvd2VyU3RhbmRpbmdzIiwiTG9jYWxQcmVzZW5jZSIsIlByZWRpY3RpdmVTY29yZXMiLCJQcmVkaWN0aXZlQ29udGFjdFNjb3JlcyJdLCJwZXJtaXNzaW9ucyI6WyJycF9tYW5hZ2Vfb3JnX3JvbGVzIiwiaWRtX21hbmFnZV91c2VycyIsInBiX21hbmFnZV9wbGF5cyIsInBiX21hbmFnZV9lbWFpbF90ZW1wbGF0ZXMiLCJwYl9zaGFyZV9lbWFpbF90ZW1wbGF0ZXMiLCJwYl9za2lwX3N0ZXBzIiwicGJfZWRpdF9hbGxfZW1haWxfdGVtcGxhdGVzIiwicGJfbWFuYWdlX2F1dG9tYXRpb25fcnVsZXMiLCJnc19nZXRfZW1haWxfcHJlZGljdGlvbiIsImdzX3VwZGF0ZV9hZ2VudF9udW1iZXIiLCJnc19jcmVhdGVfdXBkYXRlX3Bob25lX251bWJlciIsImdzX2NyZWF0ZV9lZGl0X3NhdmVkX3ZpZXdzIiwiZ3NfbmV1cmFsX2luc2lnaHRzX3JlcF9yZXBvcnRzIiwicGJfYWdlbnRfbGVnX2NsaWVudF9sZWdfZGlhbGluZyIsImdzX2FkZF92aWRlb190b19lbWFpbCJdfQ.NJIScmESY4s0hSNuediCVjcF6-IXFrgYEd4ZIiUxZtbnJb4-b0ZAu-66kA9o3qobfqHUMfgapWHZNLZr4ltyhUaiYoe45fdj4GfLsZYhcvPUX6nlW7IiCKR0CqzTSNpxbiyEX9cE-rqMWa_GjL502TzuCFpAnICb1mGtLsffw3_joQgTkhUlGPy1XHs1Eap_nsjx6xjTwmMFv1S2yi7Da_aoVR5kcNpsEVkA4m4V8_B9EXIbiIRbSSiIG9IWC02qb9jrUmI5RILXRTIB9jA7mA5S8RoGZ9Up5PclwkghVzJq1c2nBTs5V5Ig-H23PAc1-_mWZ_AOL-mI1ZrWH7yFNw"

	//run iterations
	//var iterations = 10
	//for i := 0; i < iterations; i++ {
	//	getAllLeaderboards(token)
	//	leaderboardId := createLeaderboard(token)
	//	leaderlistId := createPage(token, strconv.Itoa(leaderboardId))
	//	editPage(token, strconv.Itoa(leaderlistId))
	//	editLeaderboard(token, strconv.Itoa(leaderboardId))
	//	openPageEditScreen(token, strconv.Itoa(leaderboardId))
	//	launchLeaderboard(token, strconv.Itoa(leaderboardId))
	//	deletePage(token, strconv.Itoa(leaderlistId))
	//	deleteLeaderboard(token, strconv.Itoa(leaderboardId))
	//}

	//run time
	var done = false
	//var duration = time.Second * 120
	var i = 0
	//time.AfterFunc(duration, func() {
	//	fmt.Println("timer done")
	//	done = true
	//})
	for {
		fmt.Println("endpoint load start iteration " + strconv.Itoa(i))
		getAllLeaderboards(token)
		//leaderboardId := createLeaderboard(token)
		//leaderlistId := createPage(token, strconv.Itoa(leaderboardId))
		//editPage(token, strconv.Itoa(leaderlistId))
		//editLeaderboard(token, strconv.Itoa(leaderboardId))
		//openPageEditScreen(token, strconv.Itoa(leaderboardId))
		//launchLeaderboard(token, strconv.Itoa(leaderboardId))
		//deletePage(token, strconv.Itoa(leaderlistId))
		//deleteLeaderboard(token, strconv.Itoa(leaderboardId))
		i++
		if done {
			break
		}
	}
}

func getAllLeaderboards(token string) {
	url := "http://localhost:8082/leaderboards"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "800e519b-b7ec-49ef-9591-1f02f0d33849")

	http.DefaultClient.Do(req)
	//res, _ := http.DefaultClient.Do(req)

	//defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)
	//
	//fmt.Println(res)
	//fmt.Println(string(body))
}

func createLeaderboard(token string) int {
	url := "http://localhost:8082/leaderboards"

	payload := strings.NewReader("{\r\n\t\"Name\": \"Postman\"\r\n}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "cb469ab8-c59c-42c9-b25d-249dd4b260a0")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	//fmt.Println(res)
	//fmt.Println(string(body))
	type message struct {
		ID int
	}
	var data message
	json.Unmarshal(body, &data)

	return data.ID
}

func createPage(token string, leaderboardId string) int {
	url := "http://localhost:8082/leaderboards/171/leaderlists"

	payload := strings.NewReader("{\n\t\"LeaderboardId\": " + leaderboardId + ",\n\t\"TeamId\": \"5912396b846e1c00071cd448\",\n\t\"KpmKey\": \"email\",\n\t\"Range\": 3,\n\t\"Title\": \"New Page\"\n}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "3ff443eb-7a5a-4da5-bf13-4272266c384b")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	//fmt.Println(res)
	//fmt.Println(string(body))
	type message struct {
		ID int
	}
	var data message
	json.Unmarshal(body, &data)

	return data.ID
}

func editPage(token string, leaderlistId string) {
	url := "http://localhost:8082/leaderboards/171/leaderlists"

	payload := strings.NewReader("{\n\t\"ID\": " + leaderlistId + ",\n\t\"TeamUuid\": \"5912396b846e1c00071cd448\",\n\t\"KpmKey\": \"email\",\n\t\"Title\": \"Edited Page\",\n\t\"Range\": 3\n}")

	req, _ := http.NewRequest("PUT", url, payload)

	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "46637141-e505-4a80-9ca1-9b6b1e59414d")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)

	//fmt.Println(res)
	//fmt.Println(string(body))
}

func openPageEditScreen(token string, leaderboardId string) {
	url := "http://localhost:8082/leaderboards/" + leaderboardId + "/leaderlists"

	payload := strings.NewReader("undefined=")

	req, _ := http.NewRequest("GET", url, payload)

	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "06108223-be52-4baa-9061-8ed793222ca8")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)
	//
	//fmt.Println(res)
	//fmt.Println(string(body))
}

func editLeaderboard(token string, leaderboardId string) {
	url := "http://localhost:8082/leaderboards"

	payload := strings.NewReader("{\r\n\"Id\": " + leaderboardId + ",\r\n\"Name\": \"Edited Leaderboard\"\r\n}")

	req, _ := http.NewRequest("PUT", url, payload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "02f3d012-6e6c-499f-8bab-ddfea278eda4")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)
	//
	//fmt.Println(res)
	//fmt.Println(string(body))
}

func launchLeaderboard(token string, leaderboardId string) {
	url := "http://localhost:8082/leaderboards/leaderboard_config/" + leaderboardId

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "d7ee8158-6ca5-47b4-b2ed-ca235892cf64")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)
	//
	//fmt.Println(res)
	//fmt.Println(string(body))
}

func deletePage(token string, leaderlistId string) {
	url := "http://localhost:8082/leaderboards/171/leaderlists/" + leaderlistId

	req, _ := http.NewRequest("DELETE", url, nil)

	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "d063cfbf-3a25-4168-bcf0-3252a947c343")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)
	//
	//fmt.Println(res)
	//fmt.Println(string(body))
}

func deleteLeaderboard(token string, leaderboardId string) {
	url := "http://localhost:8082/leaderboards/" + leaderboardId

	req, _ := http.NewRequest("DELETE", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer " + token)
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "36d18795-cce5-4d00-939a-d82131da79ad")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	//body, _ := ioutil.ReadAll(res.Body)
	//
	//fmt.Println(res)
	//fmt.Println(string(body))
}
