/*
Revenge of the Pancakes
main.go file
This is my take on the revenge of the pancakes programming problem first introduced at a google code jam
https://code.google.com/codejam/contest/6254486/dashboard#s=p1
keep in mind that this code will only display the number of flips required
the input is hard coded into the program (This was not clarified in the requirements but can be changed)
*/
package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const inputString = "5\n " +
	"-\n" +
	"-+\n" +
	"+-\n" +
	"+++\n" +
	"--+-"

const tooFew = -1
const tooMany = 1
const happySideUp = "+"
const blankSideUp = "-"

func main() {
	input := inputString
	validatedInput, lengthCheck, inputError := ValidateInput(input)

	if inputError != nil {
		fmt.Println("ERROR: " + inputError.Error())
		return
	}

	//display a warning before the processing if the input was mismatched
	if lengthCheck == tooFew {
		fmt.Println("WARNING: The first line input didn't match the number of test cases provided " +
			"not enough test cases were provided.")
	}

	if lengthCheck == tooMany {
		fmt.Println("WARNING: The first line input didn't match the number of test cases provided " +
			"more data could have been processed.")
	}

	//for each string in the slice process that pancake stack
	caseNumber := 1
	for _, stack := range validatedInput {
		flips, error := ProcessString(stack)
		if error != nil {
			fmt.Println("ERROR: " + error.Error())
			return
		}
		fmt.Printf("CASE #%d: %d\n", caseNumber, flips)
		caseNumber++
	}
}

/*
ValidateInput method
This method will take a string and then validate the input to see if it meets the requirements of the project
the input must first contain the number of test cases ranging from 1-100
then the input must have that many tests cases, each line will be inserted into the string slice that will be returned
another variable called matching will be returned checking to see if the first line matches the number of test cases in the input

parameters: a valid string
returns:
	validInput, a string slice with the valid input
	lengthCheck, -1 if the first line input is less than the test cases provided, 0 if they match, 1 if the first line input is more than the test cases provided
	error, if the first line input isn't a number in the range of 1-100
*/
func ValidateInput(str string) (validInput []string, lengthCheck int, err error) {
	input := strings.Split(str, "\n")

	testCases, convertErr := strconv.Atoi(input[0])
	if convertErr != nil || (testCases < 1 || testCases > 100){
		err = errors.New("invalid input, the first line is the number of test cases ranging from 1-100")
		return
	}

	if len(input) - 1 > testCases {
		lengthCheck = tooMany
	} else if len(input) - 1 < testCases {
		lengthCheck = tooFew
		testCases = len(input) - 1
	} else {
		lengthCheck = 0
	}

	validInput = make([]string, testCases)
	i := 1
	for i <= testCases {
		validInput[i-1] = strings.TrimSpace(input[i])
		i++
	}
	return
}

/*
ProcessString method
This method will take a string made up of - and + characters and then solve the pancake flipping problem
parameters: a valid string
returns:
	flips, the minimum number of flips that pancake stack requires to get all of the pancakes happy side up, -1 if error is found
	error, if any characters are not + or - or if the length is not in the range of 1-100
*/
func ProcessString(pancakeStack string) (flips int, err error) {
	if len(pancakeStack) < 1 || len(pancakeStack) > 100 {
		err = errors.New("pancake stack height must be in range of 1-100")
		flips = -1
		return
	}

	var currentPancake string
	pancakeStackSlice := strings.Split(pancakeStack, "")

	for i, pancake := range pancakeStackSlice {
		if strings.Compare(pancake, happySideUp) != 0 && strings.Compare(pancake, blankSideUp) != 0 {
			err = errors.New("invalid character found in pancake stack")
			flips = -1
			return
		}
		if i == 0 {
			currentPancake = pancake
			flips = 1
		}
		if strings.Compare(pancake, currentPancake) != 0 {
			flips++
			currentPancake = pancake
		}
	}
	if currentPancake == happySideUp {
		flips--
	}
	return
}