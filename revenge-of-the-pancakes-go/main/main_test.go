package main

import (
	"errors"
	"strings"
	"testing"
)

func TestValidateInput(t *testing.T) {
	type expectedOutputType struct{
		validatedInput []string
		lengthCheck int
		inputError error
	}

	//test successful
	const success1 = "1\n" +
		"-+-"

	expectedOutput1 := expectedOutputType{
		[]string{"-+-"}, 0, nil,
	}

	const success5 = "5\n" +
		"-\n" +
		"+-\n" +
		"-+-\n" +
		"+++-\n" +
		"+-+-+"

	expectedOutput5 := expectedOutputType{
		[]string{"-", "+-", "-+-", "+++-", "+-+-+"}, 0, nil,
	}

	//Next Step, hard code an upper bound of 100 for a test
	const success10 = "10\n" +
		"+\n" +
		"+-\n" +
		"+-+\n" +
		"+-+-\n" +
		"+-+-+\n" +
		"+-+-+-\n" +
		"+-+-+-+\n" +
		"+-+-+-+-\n" +
		"+-+-+-+-+\n" +
		"+-+-+-+-+-"
	expectedOutput10 := expectedOutputType{
		[]string{"+", "+-", "+-+", "+-+-", "+-+-+", "+-+-+-", "+-+-+-+", "+-+-+-+-", "+-+-+-+-+", "+-+-+-+-+-"},
		0, nil,
	}

	//test incorrect first line
	const wrong = "wrong"
	expectedOutputWrong := expectedOutputType{
		nil, 0, errors.New("invalid input, the first line is the number of test cases ranging from 1-100"),
	}
	const negative = "-1"
	expectedOutputNegative := expectedOutputType{
		nil, 0, errors.New("invalid input, the first line is the number of test cases ranging from 1-100"),
	}
	const blank = ""
	expectedOutputBlank := expectedOutputType{
		nil, 0, errors.New("invalid input, the first line is the number of test cases ranging from 1-100"),
	}
	const zero = "0"
	expectedOutputZero := expectedOutputType{
		nil, 0, errors.New("invalid input, the first line is the number of test cases ranging from 1-100"),
	}
	const tooHigh = "101"
	expectedOutputTooHigh := expectedOutputType{
		nil, 0, errors.New("invalid input, the first line is the number of test cases ranging from 1-100"),
	}

	//test mismatched provided test cases
	const tooManyTestCases = "1\n" +
		"+-+\n" +
		"-+-"
	expectedOutputTooMany := expectedOutputType{
		[]string{"+-+"}, 1, nil,
	}

	const tooFewTestCases = "5\n" +
		"+-+\n" +
		"-+-"
	expectedOutputTooFew := expectedOutputType{
		[]string{"+-+", "-+-"}, -1, nil,
	}


	tables := []struct {
		input string
		expectedOutput expectedOutputType
	}{
		{success1, expectedOutput1},
		{success5, expectedOutput5},
		{success10, expectedOutput10},
		{wrong, expectedOutputWrong},
		{negative, expectedOutputNegative},
		{blank, expectedOutputBlank},
		{zero, expectedOutputZero},
		{tooHigh, expectedOutputTooHigh},
		{tooManyTestCases, expectedOutputTooMany},
		{tooFewTestCases, expectedOutputTooFew},
	}

	for _, table := range tables {
		validatedInputResult, lengthCheckResult, inputErrorResult := ValidateInput(table.input)
		if !equalStringSlices(table.expectedOutput.validatedInput, validatedInputResult){
			t.Errorf("Incorrect string slices returned, expected: %v\n got %v",
				table.expectedOutput.validatedInput, validatedInputResult)
		}
		if table.expectedOutput.lengthCheck != lengthCheckResult {
			t.Errorf("Incorrect lengthCheck match, expected: %d\n got %d",
				table.expectedOutput.lengthCheck, lengthCheckResult)
		}
		if table.expectedOutput.inputError != inputErrorResult &&
			strings.Compare(table.expectedOutput.inputError.Error(), inputErrorResult.Error()) != 0  {
			t.Errorf("Incorrect error match, expected: %s\n got %s",
				table.expectedOutput.inputError.Error(), inputErrorResult.Error())
		}
	}
}

func TestProcessString(t *testing.T) {
	type expectedOutputType struct{
		flips int
		err error
	}

	//test success
	const stack1 = "+++"
	expectedOutput1 := expectedOutputType{
		0,
		nil,
	}

	const stack2 = "----"
	expectedOutput2 := expectedOutputType{
		1,
		nil,
	}

	const stack3 = "--+-"
	expectedOutput3 := expectedOutputType{
		3,
		nil,
	}

	const stack4 = "+-+-+-+"
	expectedOutput4 := expectedOutputType{
		6,
		nil,
	}

	//test invalid characters
	const blank = ""
	expectedOutputBlank := expectedOutputType{
		-1,
		errors.New("pancake stack height must be in range of 1-100"),
	}
	const invalidChar = "-+^-+-"
	expectedOutputInvalidChar := expectedOutputType{
		-1,
		errors.New("invalid character found in pancake stack"),
	}
	const tooLong = "+++++123456789012345678901234567890123456789012345678901234567890123456789012345679801234567890102345678901234567890"
	expectedOutputTooLong := expectedOutputType{
		-1,
		errors.New("pancake stack height must be in range of 1-100"),
	}

	tables := []struct {
		input string
		expectedOutput expectedOutputType
	}{
		{stack1, expectedOutput1},
		{stack2, expectedOutput2},
		{stack3, expectedOutput3},
		{stack4, expectedOutput4},
		{blank, expectedOutputBlank},
		{invalidChar, expectedOutputInvalidChar},
		{tooLong, expectedOutputTooLong},
	}

	for _, table := range tables {
		flipsResult, errorResult := ProcessString(table.input)
		if table.expectedOutput.flips != flipsResult {
			t.Errorf("Incorrect flips returned, expected: %d\n got %d",
				table.expectedOutput.flips, flipsResult)
		}
		if table.expectedOutput.err != errorResult &&
			strings.Compare(table.expectedOutput.err.Error(), errorResult.Error()) != 0   {
			t.Errorf("Incorrect error match, expected: %s\n got %s",
				table.expectedOutput.err.Error(), errorResult.Error())
		}
	}
}

//util function for comparing string slices for equality
func equalStringSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if strings.Compare(a[i], b[i]) != 0 {
			return false
		}
	}
	return true
}
