﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace triangles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
            
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_side_a_TextChanged(object sender, TextChangedEventArgs e)
        {
            CreateOutput();
        }

        private void TextBox_side_b_TextChanged(object sender, TextChangedEventArgs e)
        {
            CreateOutput();
        }

        private void TextBox_side_c_TextChanged(object sender, TextChangedEventArgs e)
        {
            CreateOutput();
        }

        //CreateOutput method
        //called everytime some text is input into an input field
        //this will update the output field with the relevent information
        private void CreateOutput()
        {
            //validate input to be positive numbers
            double sideA = ValidateInput(textBox_side_a.Text, "A");
            if (sideA < 0.0)
            {
                return;
            }
            double sideB = ValidateInput(textBox_side_b.Text, "B");
            if (sideB < 0.0)
            {
                return;
            }
            double sideC = ValidateInput(textBox_side_c.Text, "C");
            if (sideC < 0.0)
            {
                return;
            }
            if (sideA == 0.0 || sideB == 0.0 || sideC == 0.0) 
            {
                return;
            }

            //validate input to be a triangle
            if (sideA + sideB <= sideC ||
                sideB + sideC <= sideA ||
                sideC + sideA <= sideB)
            {
                textblock_output.Text = "Those three sides can't create a triangle.";
                return;
            }


            //determine the type of triangle input
            if (sideA == sideB && sideB == sideC) {
                textblock_output.Text = "Those sides will create an equilateral triangle.";
                return;
            }

            if (sideA == sideB || sideB == sideC || sideC == sideA)
            {
                textblock_output.Text = "Those sides will create an isosceles triangle.";
                return;
            }

            //check for a right Triangle
            bool rightTriangle = false;
            double longestSide = sideA;
            if (sideB > longestSide)
            {
                longestSide = sideB;
            }
            if (sideC > longestSide)
            {
                longestSide = sideC;
            }

            if (longestSide == sideA && Math.Pow(sideB, 2) + Math.Pow(sideC, 2) == Math.Pow(sideA, 2))
            {
                rightTriangle = true;
            } else if (longestSide == sideB && Math.Pow(sideA, 2) + Math.Pow(sideC, 2) == Math.Pow(sideB, 2))
            {
                rightTriangle = true;
            } else if (longestSide == sideC && Math.Pow(sideB, 2) + Math.Pow(sideA, 2) == Math.Pow(sideC, 2))
            {
                rightTriangle = true;
            }

            if (rightTriangle)
            {
                textblock_output.Text = "Those sides will create a right triangle.";
                return;
            }

            textblock_output.Text = "Those sides will create a scalene triangle.";
        }

        //ValidateInput method
        //takes a string and tries to convert it into a useable double.
        //returns a positive double value if valid,
        //return -1 if and error is found and sets the output message accordingly.
        private double ValidateInput(string stringInput, string side)
        {
            double validInput;
            if (string.IsNullOrWhiteSpace(stringInput))
            {
                textblock_output.Text = "Enter a value for each side above.";
                return 0.0;
            }

            try
            {
                validInput = double.Parse(stringInput);
            }
            catch (FormatException e)
            {
                textblock_output.Text = "Enter a valid positive number for side " + side + ".";
                return -1.0;
            }
            catch (OverflowException e)
            {
                textblock_output.Text = "Enter a value that is closer to zero for side " + side + ".";
                return -1.0;
            }
            catch (Exception e)
            {
                textblock_output.Text = "Unknown error on side " + side + ".  Please restart the program and try again.";
                return -1.0;
            }
            if (validInput < 0.0) {
                textblock_output.Text = "Enter a valid positive number for side " + side + ".";
                return -1.0;
            }

            return validInput;
        }

    }
}
