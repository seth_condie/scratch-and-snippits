Rock, Paper, Scissors, Lizard, Spock!

This project will spin up a front end that will allow to you play rock, paper, scissors, lizard, spock.
This will also keep track of your winning streak as your score. (no penalty for a draw)

To save your score you will need to login, the scores are stored on the server, 
and just like an old NES game all of the scores are reset after the server is turned off.

This project uses react for the frontend and express for the backend.

To spin up the server, open a terminal in the backend directory of this project and run these commands:
### `npm install`
### `node api.js`
This will spin up a server on localhost port 8080.

To start the front end, open a terminal in the frontend directory of this project and run these commands:
### `npm install`
### `npm start`
This will start a development server with the frontend on localhost port 3000

In a browser go to 
### `localhost:3000`
to get started.

For more information check the readme file in the front end directory.

Future Features
1. encrypt the passwords send and stored on the backend.
2. update the front end UI to not look like a high school project.
3. add a form of persistence to the backend.
4. include snapshot tests for the front end components
5. refactor the api calls to their own file
6. refactor the login fields to their own components as well
7. refactor the front end to use the await async syntax
8. update the login process I know that there must be a better way