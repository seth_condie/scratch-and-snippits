//api.js
//this file is the back end for the RockPaperScissorsLizardSpock project
//this includes all of the endpoints and uses an array for persistence.
//like old arcade games of old when the plug is pull the scores are reset,
//so with this when the backend service is shutdown the users and their scores are lost.
//If I had more time then I would update this file to include true persistence.
//for more thoughts on other features that could have been please see the readme for this project.

//to start this part of the project open a bash window in this directory and enter the command
//$ node api.js
//this will start the server on localhost with the default port of 8080.

const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(cors());
app.options('*', cors());

//tracks what id we are on
let idCounter = 0;
//stores the user objects in an array, the id will match the index for faster information gathering.
let users = [];

//login endpoint
//this endpoint takes a body of a username and password, it will
//check each users in the storage array and see if the given username is found,
//then it will check the password, if it is a match then the full user object is returned.
//returns 400 if either the username or password isn't provided, 404 if the username isn't found,
// and 200 along with the user object for a success.
app.put('/', (req, res) => {
    if (!req.body.username || req.body.username.length < 1) {
        res.status(400).send('Please provide a username.');
        return;
    }
    if (!req.body.password || req.body.password.length < 1) {
        res.status(400).send('Please provide a password.');
        return;
    }
    //loop through get all users find one with the provided username
    for (let i = 0; i < users.length; i++) {
        if (!users[i].isDeleted && users[i].username === req.body.username) {
            if (users[i].password === req.body.password) {
                res.send(users[i]);
                return;
            }
        }
    }
    res.status(404).send("that username and password combo isn't found");
});

//get one user by id
//used mostly for debugging through postman this endpoint takes an id, and the checks
//the database for that id if found the user object is returned.
//returns 404 if the id has no user in the array, and 200 plus the user object for a successful find
app.get('/:id', (req, res) => {
    //if the user with that id exists and is not deleted then return it
    let id = req.params.id;
    if (!users[id] || users[id].isDeleted) {
        res.status(404).send('user not found with the id ' + id);
        return;
    }
    res.send(users[id]);
});

//create endpoint
//this endpoint takes a username and a password and creates a new endpoint
//the username cannot already be in the storage array
//returns 400 if the information provided in the body is invalid
//200 along with the new user object for a success
app.post('/', (req, res) => {
    //validate that all of the fields are there, add it to the array, return the user
    let newUser = {
        id: idCounter,
        username: "",
        password: "",
        highScore: 0,
        isDeleted: false
    };
    if (!req.body.username || req.body.username.length < 1) {
        res.status(400).send('Please provide a username.');
        return;
    }
    if (!req.body.password || req.body.password.length < 1) {
        res.status(400).send('Please provide a password.');
        return;
    }
    for (let i = 0; i < users.length; i++) {
        if (!users[i].isDeleted && users[i].username === req.body.username) {
            res.status(400).send('The provided username is already in use.');
            return;
        }
    }
    newUser.username = req.body.username;
    newUser.password = req.body.password;
    users[idCounter] = newUser;
    idCounter++;
    res.send(newUser);
});

//update endpoint
//I went with a patch endpoint so that I could only update the relevant information
//this endpoint takes a combination of username, password, or highScore and will updated the user object
//with the provided id
//returns 404 if the id is invalid
//returns 400 if the username that is provided is already in use
//returns 200 and the new user object for a success
app.patch('/:id', (req, res) => {
    //if a user with that id exists then validate that all of the fields are there, update it in the array, return the user
    let id = req.params.id;
    if (!users[id] || users[id].isDeleted) {
        res.status(404).send('user not found with the id ' + id);
        return;
    }
    let updatedUser = users[id];
    if (req.body.username && req.body.username.length > 0) {
        for (let i = 0; i < users.length; i++) {
            if (!users[i].isDeleted && users[i].username === req.body.username) {
                res.status(400).send('The provided username is already in use.');
                return;
            }
        }
        updatedUser.username = req.body.username;
    }
    if (req.body.password && req.body.password.length > 0) {
        updatedUser.password = req.body.password;
    }
    if (req.body.highScore && req.body.highScore > updatedUser.highScore) {
        updatedUser.highScore = req.body.highScore;
    }
    users[id] = updatedUser;
    res.send(updatedUser);
});

//delete endpoint
//I'm using soft deletes so that my idCounter idea will keep working, instead of deleting them from the array,
//otherwise the ids and the index of the array will be off
//this will take an id and mark that user as deleted
//returns 404 if that id has no user
//returns 200 if the delete was successful
app.delete('/:id', (req, res) => {
    //if a user that that id exists and it not already marked for deletion then mark it for deletion
    let id = req.params.id;
    if (!users[id] || users[id].isDeleted) {
        res.status(404).send('user not found with the id ' + id);
        return;
    }
    users[id].isDeleted = true;
    res.send('delete successful');
});

app.listen(port, function () {
    console.log(`Api listening on port ${port}.`);
});