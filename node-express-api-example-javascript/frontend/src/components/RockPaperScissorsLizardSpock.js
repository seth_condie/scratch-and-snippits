//RockPaperScissorsLizardSpock.js
//this component holds the game RockPaperScissorsLizardSpock
//this component is setup so that it can be imported and used anywhere
//it includes a callback function prop to update the score for things like a personal best or a leader board
//the pictures are stored in the assets folder and imported to this component.

import React, {Component} from 'react';
import styled from 'styled-components';
import diagram from './assets/diagram.PNG';
import rock from './assets/rock.PNG';
import paper from './assets/paper.PNG';
import scissors from './assets/scissors.PNG';
import lizard from './assets/lizard.PNG';
import spock from './assets/spock.PNG';
import PropTypes from 'prop-types';

const SCENE_START = 10;
const SCENE_ANIMATE = 20;
const SCENE_RESULTS = 30;

const ROCK = 0;
const PAPER = 1;
const SCISSORS = 2;
const LIZARD = 3;
const SPOCK = 4;

const WIN = 31;
const TIE = 32;
const LOSE = 33;

const ROCK_RESULTS = [
    {victory: TIE, message: "Tie"},
    {victory: LOSE, message: "Paper covers rock"},
    {victory: WIN, message: "Rock crushes scissors"},
    {victory: WIN, message: "Rock crushes lizard"},
    {victory: LOSE, message: "Spock vaporizes rock"}
];

const PAPER_RESULTS = [
    {victory: WIN, message: "Paper covers rock"},
    {victory: TIE, message: "Tie"},
    {victory: LOSE, message: "Scissors cut paper"},
    {victory: LOSE, message: "Lizard eats paper"},
    {victory: WIN, message: "Paper disproves Spock"}
];

const SCISSORS_RESULTS = [
    {victory: LOSE, message: "Rock crushes scissors"},
    {victory: WIN, message: "Scissors cut paper"},
    {victory: TIE, message: "Tie"},
    {victory: WIN, message: "Scissors decapitates lizard"},
    {victory: LOSE, message: "Spock smashes scissors"}
];

const LIZARD_RESULTS = [
    {victory: LOSE, message: "Rock crushes lizard"},
    {victory: WIN, message: "Lizard eats paper"},
    {victory: LOSE, message: "Scissors decapitates lizard"},
    {victory: TIE, message: "Tie"},
    {victory: WIN, message: "Lizard poisons Spock"}
];

const SPOCK_RESULTS = [
    {victory: WIN, message: "Spock vaporizes rock"},
    {victory: LOSE, message: "Paper disproves Spock"},
    {victory: WIN, message: "Spock smashes scissors"},
    {victory: LOSE, message: "Lizard poisons Spock"},
    {victory: TIE, message: "Tie"}
];

const ChoiceDiv = styled.div`
    display: flex;
    flex-direction: column;    
`;

const ChoiceImage = styled.img`
    height: 100px;
    width: 100px;
    cursor: pointer;
`;

const StyledAnimation = styled.h1`
    font-size: 90px;
    color: blue;
`;

const ResultsImage = styled.img`
    height: 100px;
    width: 100px;
`;

const StyledButton = styled.button`
    color: blue;
    background-color: white;
    border: 2px solid blue;
    cursor: pointer;
`;

const VictoryDiv = styled.div`
    font-size: 40px;
`;

const ResultsTable = styled.table`
`;

const Spacer = styled.th`
    padding: 30px;
`;


class RockPaperScissorsLizardSpock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            score: 0,
            scene: SCENE_START,
            playerSelection: 0,
            computerSelection: 0,
            resultsMessage: "",
            playerVictory: TIE
        };
    }

    gameStart = (playerSelection) => {
        this.setState({
            scene: SCENE_ANIMATE
        });
        setTimeout(() => document.getElementById("animate").innerText = "Rock,", 1000);
        setTimeout(() => document.getElementById("animate").innerText = "Paper,", 1300);
        setTimeout(() => document.getElementById("animate").innerText = "Scissors,", 1600);
        setTimeout(() => document.getElementById("animate").innerText = "Lizard,", 1900);
        setTimeout(() => document.getElementById("animate").innerText = "Spock!", 2200);
        setTimeout(() => this.showResults(playerSelection), 2700);
    };

    showResults = (playerSelection) => {
        //computer selects an option
        let computerSelection = (Math.floor(Math.random() * 4));

        //check results
        let victoryObject;
        switch (playerSelection) {
            case(ROCK):
                victoryObject = ROCK_RESULTS[computerSelection];
                break;
            case(PAPER):
                victoryObject = PAPER_RESULTS[computerSelection];
                break;
            case(SCISSORS):
                victoryObject = SCISSORS_RESULTS[computerSelection];
                break;
            case(LIZARD):
                victoryObject = LIZARD_RESULTS[computerSelection];
                break;
            default:
                victoryObject = SPOCK_RESULTS[computerSelection];
                break;
        }

        //adjust score
        let newScore = 0;
        if (victoryObject.victory === WIN) {
            newScore = this.state.score + 1;
        } else if (victoryObject.victory === TIE) {
            newScore = this.state.score;
        }

        this.props.scoreNotify(newScore);

        this.setState({
            scene: SCENE_RESULTS,
            score: newScore,
            playerSelection: playerSelection,
            computerSelection: computerSelection,
            resultsMessage: victoryObject.message,
            playerVictory: victoryObject.victory
        });
    };

    playAgain = () => {
        this.setState({
           scene: SCENE_START
        });
    };

    playerResultsDisplay = () => {
        switch (this.state.playerSelection) {
            case(ROCK):
                return (<ResultsImage src={rock} alt={'player chose rock'}/>);
            case(PAPER):
                return (<ResultsImage src={paper} alt={'player chose paper'}/>);
            case(SCISSORS):
                return (<ResultsImage src={scissors} alt={'player chose scissors'}/>);
            case(LIZARD):
                return (<ResultsImage src={lizard} alt={'player chose lizard'}/>);
            case(SPOCK):
                return (<ResultsImage src={spock} alt={'player chose Spock'}/>);
            default:
                return (<ResultsImage src={diagram} alt={'internal error, player selection'}/>);
        }
    };

    computerResultsDisplay = () => {
        switch (this.state.computerSelection) {
            case(ROCK):
                return (<ResultsImage src={rock} alt={'computer chose rock'}/>);
            case(PAPER):
                return (<ResultsImage src={paper} alt={'computer chose paper'}/>);
            case(SCISSORS):
                return (<ResultsImage src={scissors} alt={'computer chose scissors'}/>);
            case(LIZARD):
                return (<ResultsImage src={lizard} alt={'computer chose lizard'}/>);
            case(SPOCK):
                return (<ResultsImage src={spock} alt={'computer chose Spock'}/>);
            default:
                return (<ResultsImage src={diagram} alt={'internal error, computer selection'}/>);
        }
    };

    getVictoryDeclaration = () => {
        if (this.state.playerVictory === WIN) {
            return (
                <VictoryDiv>You Win</VictoryDiv>
            )
        } else if (this.state.playerVictory === TIE) {
            return (
                <div></div>
            )
        }
        return (
            <VictoryDiv>You Lost</VictoryDiv>
        )
    };

    getBody = () => {
        switch (this.state.scene) {
            case(SCENE_START):
                return (
                    <ChoiceDiv>
                        <h3>Choose Wisely</h3>
                        <ChoiceImage src={rock} alt={'rock'} onClick={() => this.gameStart(ROCK)}/>
                        <ChoiceImage src={paper} alt={'paper'} onClick={() => this.gameStart(PAPER)}/>
                        <ChoiceImage src={scissors} alt={'scissors'} onClick={() => this.gameStart(SCISSORS)}/>
                        <ChoiceImage src={lizard} alt={'lizard'} onClick={() => this.gameStart(LIZARD)}/>
                        <ChoiceImage src={spock} alt={'spock'} onClick={() => this.gameStart(SPOCK)}/>
                    </ChoiceDiv>
                );
            case(SCENE_ANIMATE):
                return (
                    <div>
                        <StyledAnimation id="animate">Ready...</StyledAnimation>
                    </div>
                );
            case(SCENE_RESULTS):
                const playerResults = this.playerResultsDisplay();
                const computerResults = this.computerResultsDisplay();
                const victoryDeclaration = this.getVictoryDeclaration();
                return (
                    <div>
                        <ResultsTable>
                            <thead>
                            <tr>
                                <th colSpan={"3"}>{victoryDeclaration}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th><label>You Chose</label></th>
                                <Spacer ></Spacer>
                                <th><label>Com Chose</label></th>
                            </tr>
                            <tr>
                                <th>{playerResults}</th>
                                <Spacer ></Spacer>
                                <th>{computerResults}</th>
                            </tr>
                            <tr>
                                <th colSpan={"3"}>{this.state.resultsMessage}</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.playAgain}>play again</StyledButton></th>
                            </tr>
                            </tbody>
                        </ResultsTable>
                    </div>
                );
            default:
                return (
                    <div>
                        An unknown error occurred, please restart the app.
                    </div>
                );
        }
    };

    render() {
        const body = this.getBody();
        return (
            <div>
                {body}
            </div>
        );

    }
}

RockPaperScissorsLizardSpock.propTypes = {
  scoreNotify: PropTypes.func
};

export default RockPaperScissorsLizardSpock;