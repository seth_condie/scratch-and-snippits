//App.js
//this file is the home base of this project it uses a state system to track what should be displayed at the proper time.
//There are four pages, login, register, edit, and play, the state will track where you are and where you can move to.
//I'm using mostly styled components for the style to keep everything all in one file and I'm using a combination of react
//and vanilla javascript to reduce the number of times that the page needs to rerender, if this needed to scale up then
//it would be better to refactor it to use more react and less vanilla javascript.

//to start this part of the project open a bash window in this directory and enter this command
//$ npm start
//this will start a development server and load the front end on localhost port 3000, this file is setup to call the backend on port 8080
//this can be changed with the port const below

import React, {Component} from 'react';
import styled from 'styled-components';
import RockPaperScissorsLizardSpock from './components/RockPaperScissorsLizardSpock';
import diagram from "./components/assets/diagram.PNG";

const STATE_LOGIN = 1;
const STATE_REGISTER = 2;
const STATE_EDIT = 3;
const STATE_PLAY = 4;

const BASE_URL = "http://localhost:";
const PORT = "8080/";

const HeaderDiv = styled.div`
    display: flex;
    justify-content: center;
    color: blue;
`;

const LoginDivWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

const PaddedSpan = styled.div`
    padding: 10px;
`;

const LoginCredentials = styled.table`
    padding: 10px;
`;

const StyledButton = styled.button`
    color: blue;
    background-color: white;
    border: 2px solid blue;
    cursor: pointer;
`;

const SplitContainerDiv = styled.div`
    display: flex;
`;

const UserDisplayDiv = styled.div`
    flex: 1;
    display: flex;
    justify-content: flex-end;
    padding-right: 200px;
`;

const DiagramImage = styled.img`
    height: 300px;
    width: 300px;
`;

const GameDiv = styled.div`
    flex: 1;
`;

const ErrorOutput = styled.span`
    color: red;
`;

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayState: STATE_LOGIN,
            openId: -1,
            openUsername: '',
            personalBest: 0,
            currentScore: 0
        };
    }

    //attemptLogin
    //this will verify that a username and password have been entered into the fields
    //then call the login endpoint and see if the login is a success
    //error messages are displayed on screen and in the console window
    //on success you are moved to the play page
    //by clicking the register button you are taken to the register page
    attemptLogin = () => {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        if (username.length < 1 || password.length < 1) {
            document.getElementById("error").innerText = "Please enter a username and password to login.";
            return;
        }
        let loginBody = {
            username: username,
            password: password,
        };
        fetch(BASE_URL + PORT, {
            method: "put",
            body: JSON.stringify(loginBody),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(response => {
            if (response.status !== 200){
                console.error('Error logging in response code ' + response.status);
                response.text().then(text => console.error('response text: ' + text));
                document.getElementById("error").innerText = "That username and password combo cannot be found.";
            } else {
                //login successful
                response.json().then(data => {
                    this.setState({
                        displayState: STATE_PLAY,
                        openId: data.id,
                        openUsername: data.username,
                        personalBest: data.highScore,
                        currentScore: 0
                    })
                });
            }
        }).catch(err => {
            console.error('Error attempting to login.');
            console.error(err);
            document.getElementById("error").innerText = "That username and password combo cannot be found.";
        });
    };

    //registerNewUser
    //called from the register page this will take a username and password validate them and then call then api
    //to create a new user with that information.
    //errors are display in the browser window and the console.
    //on success you are taken to the login screen, hitting the cancel button will take you back to the login screen.
    registerNewUser = () => {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        let password2 = document.getElementById("password2").value;
        if (username.length < 1) {
            document.getElementById("error").innerText = "Please enter a username.";
            return;
        }
        if (password.length < 1 ) {
            document.getElementById("error").innerText = "Please enter a password.";
            return;
        }
        if (password !== password2) {
            document.getElementById("error").innerText = "Your passwords don't match, please reenter your password.";
            return;
        }
        //call the backend
        let newUser = {
            username: username,
            password: password,
        };
        fetch(BASE_URL + PORT, {
            method: "post",
            body: JSON.stringify(newUser),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(response => {
            if (response.status !== 200){
                console.error('Error creating a new user response code ' + response.status);
                response.text().then(text => document.getElementById("error").innerText = text);
            } else {
                //create successful
                response.json().then(data => {
                    this.setState({
                        displayState: STATE_PLAY,
                        openId: data.id,
                        openUsername: data.username,
                        personalBest: data.highScore,
                        currentScore: 0
                    })
                });
            }
        }).catch(err => {
            console.error('Error creating a new user.');
            console.error(err);
        });
    };

    //updateUser
    //this method is called by the edit screen
    //the username and password are validated and then send to the backend to update the storage
    //you cannot edit your username to be a username that is already in the system.
    //on success you are taken back to the play page.
    updateUser = () => {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        let password2 = document.getElementById("password2").value;
        if (username.length < 1 && password.length < 1) {
            document.getElementById("error").innerText = "Please fill in the fields you would like to edit, or click cancel.";
            return;
        }
        if (password !== password2) {
            document.getElementById("error").innerText = "Your passwords don't match, please reenter your password.";
            return;
        }
        let updateUser = {
            id: this.state.openId
        };
        if (password.length > 0) {
            updateUser.password = password;
        }
        if (username.length > 0) {
            updateUser.username = username;
        }
        //call the backend
        fetch(BASE_URL + PORT + this.state.openId, {
            method: "patch",
            body: JSON.stringify(updateUser),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(response => {
            if (response.status !== 200){
                console.error('Error updating a user response code ' + response.status);
                response.text().then(text => console.error('response text: ' + text));
            } else {
                //update successful
                response.json().then(data => {
                    this.setState({
                        displayState: STATE_PLAY,
                        openId: data.id,
                        openUsername: data.username,
                        personalBest: data.highScore
                    })
                });
            }
        }).catch(err => {
            console.error('Error updating a user.');
            console.error(err);
        });
    };

    //deleteUser
    //called from the edit page this will delete the open user and take you back to the login screen
    deleteUser = () => {
        fetch(BASE_URL + PORT + this.state.openId, {
            method: "delete",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(response => {
            if (response.status !== 200){
                console.error('Error deleting a user response code ' + response.status);
                response.text().then(text => console.error('response text: ' + text));
            } else {
                //delete successful
                this.setState({
                    displayState: STATE_LOGIN,
                    openId: -1,
                    openUsername: "",
                    personalBest: "",
                    currentScore: 0
                });
            }
        }).catch(err => {
            console.error('Error deleting a user.');
            console.error(err);
        });
        this.toLogin();
    };

    //scoreUpdate
    //called from the play page, this will update the score on screen and update your new personal best if needed.
    //in this case the update patch endpoint will be called.
    //the ui will updated with the new scores on success.
    scoreUpdate = (newScore) => {
        let newPersonalBest = this.state.personalBest;
        if (newScore > newPersonalBest) {
            newPersonalBest = newScore;
            //call the backend
            let updateUser = {
                id: this.state.openId,
                personalBest: newPersonalBest
            };
            fetch(BASE_URL + PORT + this.state.openId, {
                method: "patch",
                body: JSON.stringify(updateUser),
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(response => {
                if (response.status !== 200){
                    console.error('Error updating a personal best, response code ' + response.status);
                    response.text().then(text => console.error('response text: ' + text));
                } else {
                    //update successful
                }
            }).catch(err => {
                console.error('Error updating a personal best.');
                console.error(err);
            });
        }
        this.setState({
            currentScore: newScore,
            personalBest: newPersonalBest
        });
    };

    //this will move to the register page
    toRegister = () => {
        if (document.getElementById("error")) {
            document.getElementById("error").innerText = "";
        }
        this.setState({
            displayState: STATE_REGISTER
        });
    };

    //this will move to the play page
    toPlay = () => {
        this.setState({
            displayState: STATE_PLAY
        });
    };

    //this will move to the login page
    toLogin = () => {
        if (document.getElementById("error")) {
            document.getElementById("error").innerText = "";
        }
        this.setState({
            displayState: STATE_LOGIN
        });
    };

    //this will move to the edit page
    toEdit = () => {
        if (document.getElementById("error")) {
            document.getElementById("error").innerText = "";
        }
        this.setState({
            displayState: STATE_EDIT
        });
    };

    getVideo = () => {
        return (
            <iframe title={"howToPlay"}
                    width="560" height="315"
                    src="https://www.youtube.com/embed/cSLeBKT7-sM"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen>
            </iframe>
        )
    };

    getBody = () => {
        const video = this.getVideo();
        switch (this.state.displayState) {
            case STATE_LOGIN:
                return (
                    <LoginDivWrapper>
                        <div>
                            {video}
                        </div>
                        <PaddedSpan>Welcome! Please login or <StyledButton
                            onClick={this.toRegister}>register.</StyledButton></PaddedSpan>
                        <LoginCredentials>
                            <thead>
                            <tr>
                                <th><label>username: </label></th>
                                <th><input id={"username"} type={"text"} name={"username"}></input></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th><label>password: </label></th>
                                <th><input id={"password"} type={"password"} name={"password"}></input></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.attemptLogin}>login</StyledButton></th>
                            </tr>
                            </tbody>
                        </LoginCredentials>
                        <ErrorOutput id={"error"}></ErrorOutput>
                    </LoginDivWrapper>
                );
            case STATE_REGISTER:
                return (
                    <LoginDivWrapper>
                        <div>
                            {video}
                        </div>
                        <PaddedSpan>Please enter your information and click the register button.</PaddedSpan>
                        <LoginCredentials>
                            <thead>
                            <tr>
                                <th><label>username: </label></th>
                                <th><input id={"username"} type={"text"} name={"username"}></input></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th><label>password: </label></th>
                                <th><input id={"password"} type={"password"} name={"password"}></input></th>
                            </tr>
                            <tr>
                                <th><label>repeat password: </label></th>
                                <th><input id={"password2"} type={"password"} name={"password"}></input></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.registerNewUser}>register</StyledButton></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.toLogin}>cancel</StyledButton></th>
                            </tr>
                            </tbody>
                        </LoginCredentials>
                        <ErrorOutput id={"error"}></ErrorOutput>
                    </LoginDivWrapper>
                );
            case STATE_EDIT:
                return (
                    <LoginDivWrapper>
                        <div>
                            {video}
                        </div>
                        <PaddedSpan>Please enter in all field you would like to edit then click the save button.</PaddedSpan>
                        <LoginCredentials>
                            <thead>
                            <tr>
                                <th><label>username: </label></th>
                                <th><input id={"username"} type={"text"} name={"username"}></input></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th><label>password: </label></th>
                                <th><input id={"password"} type={"password"} name={"password"}></input></th>
                            </tr>
                            <tr>
                                <th><label>repeat password: </label></th>
                                <th><input id={"password2"} type={"password"} name={"password"}></input></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.updateUser}>save</StyledButton></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.toPlay}>cancel</StyledButton></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th align="right"><StyledButton onClick={this.deleteUser}>delete account</StyledButton></th>
                            </tr>
                            </tbody>
                        </LoginCredentials>
                        <ErrorOutput id={"error"}></ErrorOutput>
                    </LoginDivWrapper>
                );
            case STATE_PLAY:
                return (
                    <SplitContainerDiv>
                        <UserDisplayDiv>
                            <LoginCredentials>
                                <thead>
                                <tr>
                                    <td><label>username: </label></td>
                                    <td><h3>{this.state.openUsername}</h3></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><label>personal best: </label></td>
                                    <td><h3>{this.state.personalBest}</h3></td>
                                </tr>
                                <tr>
                                    <td><label>current score: </label></td>
                                    <td><h3>{this.state.currentScore}</h3></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right"><StyledButton onClick={this.toEdit}>edit profile</StyledButton></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right"><StyledButton onClick={this.toLogin}>logout</StyledButton></td>
                                </tr>
                                <tr>
                                    <td colSpan={"2"} align="right"><DiagramImage src={diagram} alt={'help diagram'}/></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </LoginCredentials>
                        </UserDisplayDiv>
                        <GameDiv>
                            <RockPaperScissorsLizardSpock
                            scoreNotify={this.scoreUpdate}/>
                        </GameDiv>
                    </SplitContainerDiv>
                );
            default:
                return (
                    <div>
                        An unknown error occurred, please restart the app.
                    </div>
                );
        }
    };

    render() {
        const body = this.getBody();
        return (
            <div>
                <HeaderDiv>
                    <h1>Rock, Paper, Scissors, Lizard, Spock!</h1>
                </HeaderDiv>
                {body}
            </div>
        );
    }
}

export default App;
